% TODO: Andrei Bogdan Alexandru 324CA

:- ensure_loaded('T3t.pl').
:- ensure_loaded('T3base.pl').
:- dynamic next_tile/2.


% initial_game_state(-GameState).
% Întoarce în GameState starea inițială a jocului.
initial_game_state([]).
% Momentan adaugam doar o lista cu piesele puse in joc.
% Piesa = ((0,0),'#0','R0')
% Vedem pe viitor daca mai e nevoie de ceva (eventual de trasee pregenerate)


% get_game_tiles(+GameState, -PlacedTileList)
% Întoarce lista de cărți rotite plasate în joc.
% Lista este o listă de triplete conținând:
% - coordonatele (X, Y) ale spațiului unde este cartea
% - identificatorul cărții (#1..#10)
% - identificatorul rotației (R0..R3)

get_game_tiles([], []).
get_game_tiles(GameState, Paths) :- Paths is GameState.
% Momentan intoarce doar lista de piese direct.

% get_open_paths(+GameState, -Paths)
% Întoarce în Paths o listă de trasee care pornesc
% dintr-un punct de intrare și nu ajung într-un punct
% de ieșire. Elementele din fiecare traseu vor fi citite
% folosind predicatele get_path_*

get_open_paths([],[]).

% get_closed_paths(+GameState, -ClosedPaths)
% Întoarce în ClosedPaths traseele care pornesc
% dintr-un punct de intrare și ajung într-un punct
% de ieșire. Elementele din fiecare traseu vor fi citite
% folosind predicatele get_path_*
get_closed_paths([],[]).
get_closed_paths(GameState,Paths).


% get_path_entry(+Path, -Point)
% Întoarce în Point o pereche (X, Y) care este punctul
% de intrare pentru un traseu. Pentru X și Y întoarse
% trebuie ca entry_point(X, Y, _) să fie adevărat.
get_path_entry([],[]).
get_path_entry(Path, Point) :- [Point|_] is Path.

% get_path_tiles(+Path, -IDs)
% Întoarce în IDs o listă de perechi (TID, RID)
% care conține, în ordinea în care sunt parcurse de traseu,
% pentru fiecare carte parcursă identificatorul cărții și
% identificatorul rotației acesteia.
% Este posibil ca o carte să apară în traseu de mai multe ori,
% dacă traseul trece de mai multe ori prin ea.
get_path_tiles([],[]).



% available_move(+GameState, -Move)
% Predicatul leagă argumentul Move la o mutare disponibilă
% în starea GameState a jocului. Formatul pentru Move trebuie
% să fie același ca și în cazul utilizării predicatelor
% get_move_*.
% Dacă Move este legat, predicatul întoarce true dacă mutarea
% este disponibilă.
% Dacă Move nu este legat, soluții succesive (obținute folosind
% comanda ; la consolă, sau obținute cu findall) vor oferi
% diverse mutări valide în starea de joc dată.
% findall(Move, available_move(State, Move), Moves) trebuie să
% întoarcă în Moves toate mutările valide în starea dată, fără
% duplicate.

% Adaugam si predicate pentru piese + rotatiile lor
% Piece = (Piesa, Rotatie)
piece('#1', 'R0').
piece('#2', 'R0').
piece('#2', 'R1').
piece('#2', 'R2').
piece('#2', 'R3').
piece('#3', 'R0').
piece('#3', 'R1').
piece('#3', 'R2').
piece('#3', 'R3').
piece('#4', 'R0').
piece('#4', 'R1').
piece('#5', 'R0').
piece('#5', 'R1').
piece('#5', 'R2').
piece('#5', 'R3').
piece('#6', 'R0').
piece('#7', 'R0').
piece('#7', 'R1').
piece('#7', 'R2').
piece('#7', 'R3').
piece('#8', 'R0').
piece('#8', 'R1').
piece('#9', 'R0').
piece('#10', 'R0').

is_margin(Place) :- X/Y = Place, limits(X0,Y0,W,H),
			(Place == (X0/Y); Place == (H/Y); Place == (X/Y0); Place == (X/W)).

% Verifica daca putem pune o piesa pe o pozitie din margine
% Atentie! Urmeaza o mare "hardcodare". Punem toate cazurile nefericite.
is_valid_margin(Move) :- (X/Y, P/R) = Move, limits(X0,Y0,W,H),
			Place = X/Y, is_margin(Place),
			\+ (P == '#1'), % Nu au cum sa fie in margine deloc
			% Tratam coltul stanga sus
			\+ (X == 1, Y == 1,((P == '#2', R == 'R1');
								(P == '#3', (R == 'R1'; R == 'R2'));
								(P == '#5', (R == 'R1'; R == 'R2'));
								(P == '#6');
								(P == '#7', (R == 'R1'; R == 'R3'));
								(P == '#8', (R == 'R1'; R == 'R3'));
								(P == '#10'))),
			% Tratam coltul dreapta sus
			\+ (X == 1, Y == W,((P == '#2', R == 'R0');
								(P == '#3', (R == 'R0'; R == 'R1'));
								(P == '#5', (R == 'R0'; R == 'R1'));
								(P == '#6');
								(P == '#7', (R == 'R0'; R == 'R2'));
								(P == '#8', (R == 'R0'; R == 'R2'));
								(P == '#10'))),
			% Tratam coltul dreapta jos
			\+ (X == H, Y == W,((P == '#2', R == 'R3');
								(P == '#3', (R == 'R0'; R == 'R3'));
								(P == '#5', (R == 'R0'; R == 'R3'));
								(P == '#6');
								(P == '#7', (R == 'R1'; R == 'R3'));
								(P == '#8', (R == 'R1'; R == 'R3'));
								(P == '#10'))),
			% Tratam coltul stanga jos
			\+ (X == H, Y == 1, ((P == '#2', R == 'R2');
								(P == '#3', (R == 'R2'; R == 'R3'));
								(P == '#5', (R == 'R2'; R == 'R3'));
								(P == '#6');
								(P == '#7', (R == 'R0'; R == 'R2'));
								(P == '#8', (R == 'R0'; R == 'R2'));
								(P == '#10'))),
			% Tratam linia de sus
			\+ (X == 1, Y >= 1, Y =< W,((P == '#2', (R == 'R2'; R == 'R3'));
										(P == '#3',  R == 'R3');
										(P == '#4', (R == 'R1'; R == 'R3'));
										(P == '#5',  R == 'R3'))),
			% Tratam linia din dreapta
			\+ (X >= 1, X =< H, Y == W,((P == '#2', (R == 'R1'; R == 'R2'));
										(P == '#3',  R == 'R2');
										(P == '#4', (R == 'R0'; R == 'R2'));
										(P == '#5',  R == 'R2'))),
			% Tratam linia de jos
			\+ (X == H, Y >= 1, Y =< W,((P == '#2', (R == 'R1'; R == 'R2'));
										(P == '#3',  R == 'R1');
										(P == '#4', (R == 'R1'; R == 'R3'));
										(P == '#5',  R == 'R1'))),
			% Tratam linia din stanga
			\+ (X >= 1, X =< H, Y == 1,((P == '#2', (R == 'R0'; R == 'R3'));
										(P == '#3',  R == 'R0');
										(P == '#4', (R == 'R0'; R == 'R2'));
										(P == '#5',  R == 'R0'))).

generate_position(Place) :-  limits(X0,Y0,W,H),
			between(X0, W, X), between(Y0, H, Y), Place = X/Y.
%% generate_position(Place) :-  limits(X0,Y0,W,H),
%% 			between(X0, W, X), Place = X/X.

generate_piece(Piece) :- piece(P,R), Piece = P/R.

% Daca Move nu este legat generam o piesa si veriifcam toate locurile
% in care o putem pune.
available_move(GameState,Move) :- var(Move), 
			
			generate_position(Place),generate_piece(Piece),
			Move = (Place, Piece),
			is_valid_margin(Move).



is_in_limits(Place) :- X/Y = Place, limits(X0,Y0,W,H),
			(X >= X0 , X =< W), (Y >= Y0, Y =< H).

%% available_move(GameState,Move) :- nonvar(Move), is_in_limits(Move),
%% 				\+ member(Move, GameState) , is_margin(Move).
% (X1,Y1) coltul stanga sus; (X2,Y2), coltul dreapta jos.

				

% Atenție! Folosirea celor 3 predicate get_move_* pe aceeași
% variabilă neinstanțiată Move trebuie să rezulte în legarea
% lui Move la o descriere completă a mutării, astfel încât
% available_move(FS, Move) să dea adevărat dacă mutarea
% descrisă este validă în starea GS a jocului.

% get_move_space(?Move, ?Space)
% Predicatul este adevărat dacă Space corespunde spațiului
% (X, Y) de pe hartă unde a fost pusă o carte în urma
% mutării Move.
% Vezi și observația de mai sus.
get_move_space(_,_) :- fail.

% get_move_tile_id(?Move, ?TID)
% Predicatul este adevărat dacă TID corespunde
% identificatorului ('#1'..'#10') cărții care a fost plasată
% pe hartă în urma mutării Move.
% Vezi și observația de mai sus.
get_move_tile_id(_,_) :- fail.

% get_move_rotation_id(?Move, ?RotID)
% Predicatul este adevărat dacă RotID corespunde
% identificatorului rotației ('R0'..'R3') cărții care a fost
% plasată în urma mutării Move.
% Vezi și observația de mai sus.
get_move_rotation_id(_,_) :- fail.



% apply_move(+GameStateBefore, +Move, -GameStateAfter)
% Leagă al treilea argument la starea de joc care rezultă
% în urma aplicării mutării Move în starea GameStateBefore.
apply_move(GameStateBefore,Move,GameStateAfter) :- append([Move], GameStateBefore, GameStateAfter).

% pick_move(+GameState, +TID, -Move)
% Alege o mutare care folosește cartea cu identificatorul TID,
% pentru a fi aplicată în starea GameState. Mutarea este
% întoarsă în Move.
pick_move(_,_,_) :- fail.


% play_game(-FinalGameState)
% Joacă un joc complet, pornind de la starea inițială a jocului
% și continuând până când nu se mai poate pune cartea care
% este la rând.
% Cărțile de plasat se obțin folosind
% predicatul next_tile(+Time, -TID), unde Time este legat la
% numărul de mutări realizate până în momentul curent, iar
% predicatul next_tile va lega TID la un identificator de carte.
play_game(_) :- fail.



















































